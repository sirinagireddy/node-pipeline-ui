﻿
(function () {
  'use strict';

  angular
    .module('app')
    .controller('homeController', CatalogController);

  CatalogController.$inject = ['$http', '$scope', '$location'];

  function CatalogController($http, $scope, $location) {
    var vm = this;

    function getAllUsersSuccess(response){
      $scope.getAllUsers = response.data;
      console.log("getAllUsersSuccess : ", response);
    }

    function getAllUsersError(response){
      console.log("getAllUsersError : ", response);
      $scope.getAllUsers = response.data;
    }

    vm.init = function () {
      vm.message = 'Hello World';
      getAllUsers().then(getAllUsersSuccess, getAllUsersError);
    };

    $scope.submit = function () {
      console.log("Submit called");
      //alert("submit called");
      $location.path('login');
    };

    function getAllUsers() {
			console.log("getAllUsers");
			return $http.get('https://node-pipeline-example.herokuapp.com/api/GetAllUsers')
				.then(function(response) {
					return response;
				});
		}

    //vm.init();
  }
}());


(function () {
  'use strict';

  angular
    .module('app')
    .controller('loginController', CatalogController);

  CatalogController.$inject = ['$http', '$scope'];

  function CatalogController($http, $scope) {
    var vm = this;
    $scope.isadmin = false;
    $scope.isuser = false;
    $scope.isloginSuccess = true;

    function loginSuccess(response){
      $scope.userdata = response.data[0];
      $scope.isloginSuccess = false;
      $scope.title = "Hello 3Tier App";
      if($scope.userdata.role == 'admin'){
        $scope.isadmin = true;
      }else{
        $scope.isuser=true;
      }
      
      console.log("getAllUsersSuccess : ", response);
    }

    function loginError(response){
      console.log("getAllUsersError : ", response);
      $scope.userdata = response.data;
    }

    $scope.submit = function () {
      $scope.inputJson = {
        "emailid" : $scope.emailid,
        "password" : $scope.password
      };

      login($scope.inputJson).then(loginSuccess, loginError);
    };

    $scope.logout = function () {
      $scope.isloginSuccess = true;
      $scope.userdata = '';
    };

    function login(input) {
			return $http.post('https://node-pipeline-example.herokuapp.com/api/Login', input)
				.then(function(response) {
					return response;
				});
		}

    //vm.init();
  }
}());

const webdriver = require('selenium-webdriver');

const USERNAME = 'sreekar.teegala';

const KEY = 'MMEV02jlkJZCwXSrtp3wsAHQnnPBYW1FlUN8lsttY2e1qjmm47';


// gridUrl: gridUrl can be found at automation dashboard
const GRID_HOST = 'hub.lambdatest.com/wd/hub';



function apptest(){

    var capabilities = {
        name: 'Test 1', // name of the test
        build: 'NodeJS build', // name of the build
        platform : "Windows 10",
        browserName : "Chrome",
        version : "75.0",
        visual : true,
        resolution: '1280x800',
        network: true,
        console: true,
    }

    console.log("capabilities : ", capabilities);

    const gridUrl = 'https://' + USERNAME + ':' + KEY + '@' + GRID_HOST;

    console.log("gridUrl : ", gridUrl);
    var driver = new webdriver.Builder().forBrowser('chrome').build();

    	
    //driver.sleep(50000000000000000000000000);
    // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    // const driver = new webdriver.Builder()
    //         .usingServer(gridUrl)
    //         .withCapabilities(capabilities)
    //         .build();


    let action = driver.actions();
    
    console.log("waiting");    
    //action.pause(200000).perform();
    driver.sleep(20000).then(function(){
        driver.get('http://localhost:5000').then(function() {
            console.log("site called");
            action.pause(2000).perform();
            driver.findElement(webdriver.By.name('loginbtn')).click().then(function(){
                console.log("loginbtn clicked");
                action.pause(2000).perform();
                driver.findElement(webdriver.By.name('login')).sendKeys('sreekar@gmail.com').then(function(){
                    console.log("login clicked");
                    driver.findElement(webdriver.By.name('password')).sendKeys('welcome').then(function(){
                        console.log("password clicked");
                        driver.findElement(webdriver.By.className('fadeIn fourth')).click().then(function(){
                            console.log("Login submit clicked");
                            action.pause(5000).perform();
                            driver.findElement(webdriver.By.id('logout')).click().then(function(){
                                console.log("logout clicked");
                                setTimeout(function() {
                                    console.log("Test Success");
                                    driver.quit();
                                }, 5000);
                            }).catch(function(error){
                                console.log("webdriver.By.id('logout') failed " + error)
                                driver.quit();
                            });
                        }).catch(function(err){
                            console.log("webdriver.By.className('fadeIn fourth') failed" + err)
                            driver.quit();
                        });
                    }).catch(function(ex){
                        console.log("webdriver.By.name('password') failed " + ex)
                        driver.quit();
                    });
                }).catch(function(e){
                    console.log("webdriver.By.name('login') failed " + e)
                    driver.quit();
                });
            }).catch(function(exception){
                console.log("webdriver.By.name('loginbtn') failed " + exception)
                driver.quit();
            });
        }).catch(function(exe){
            console.log("test failed with reason " + exe)
            driver.quit();
        });
    }).catch(function(sleepex){
        console.log("Sleep Error: ", sleepex);
    })
    
}
apptest();